#include "fallen.h"

#define IMG_WIDTH 32
#define IMG_HEIGHT 32

fallen::fallen() {
}

fallen::fallen(float x, float y, float radius, float mass) {
    this->position.x = x;
    this->position.y = y;
    this->radius = radius;
    this->mass = mass;
//    this->frictionCoefficient = frictionCoefficient;

	lastPos.x = 0;
	lastPos.y = 0;
}

void fallen::update() {
    velocity += acceration;

    if(velocity.length() > MAX_LENGTH){
        velocity.normalize();
        velocity *= MAX_LENGTH;
    }

    position += velocity;
    acceration *= 0;
//    printf("%f", velocity.x);
}

void fallen::applyForce(ofVec2f force) {
    //printf("Applying force!");
    acceration = acceration + (force/mass);
}

void fallen::draw(ofColor color) {
    //ofSetColor(210);
	ofEnableBlendMode(OF_BLENDMODE_ADD);	
    ofSetColor(color);

	ofCircle(position.x, position.y, radius);
	ofLine(lastPos.x, lastPos.y, position.x, position.y);
	lastPos = ofVec2f(position.x, position.y);	
	ofDisableBlendMode();
}



void fallen::draw(ofImage tex) {	
	ofEnableBlendMode(OF_BLENDMODE_ADD);	
	//tex.setAnchorPercent(0.5, 0.5);
	tex.draw(position.x, position.y, IMG_WIDTH, IMG_HEIGHT);	
	//ofCircle(position.x, position.y, radius);
	ofDisableBlendMode();
}

void fallen::stop(){
    velocity *= 0;
}

ofVec2f fallen::getVelocity() {
    return velocity;
}

float fallen::getMass() {
    return mass;
}

ofVec2f fallen::getPosition() {
    return position;
}

void fallen::edgeBounce() {
    if (position.x > ofGetWindowWidth()) {
      velocity.x *= -1;
    }
    else if (position.x < 0) {
      velocity.x *= -1;
    }

    if (position.y > ofGetWindowHeight()) {
      velocity.y *= -1;
    }
    else if (position.y < 0) {
      velocity.y *= -1;
    }
}

void fallen::checkEdge() {
    if (position.x > ofGetWindowWidth()) {
      position.x = 0;
	  lastPos.x = 0;
    }
    else if (position.x < 0) {
      position.x = ofGetWindowWidth();
      lastPos.x = ofGetWindowWidth();
    }

    if (position.y > ofGetWindowHeight()) {
      position.y = 0;
      lastPos.y = 0;
    }
    else if (position.y < 0) {
      position.y = ofGetWindowHeight();
      lastPos.y = ofGetWindowHeight();
    }
}
