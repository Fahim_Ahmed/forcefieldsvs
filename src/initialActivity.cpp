#include "initialActivity.h"

#define FRICTION_COEEFFICIENT 0.1
#define IMG_NAME "grad.png"
#define PARTICLE_TEX "glowingDot.png"
#define IMG_WIDTH 1000
#define IMG_HEIGHT 200

int forceMode = 0;

void initialActivity::setup(){
    ofSetFrameRate(30);
	ofBackground(0);
	//ofEnableSmoothing();
//	ofSetCoordHandedness(OF_LEFT_HANDED);
	postFx.init(ofGetWindowWidth(), ofGetWindowHeight());
	postFx.createPass<BloomPass>();

	forceOn = false;
    frictionCoefficient = FRICTION_COEEFFICIENT;
	_forceField = new forceField(35);
	gradImage.loadImage(IMG_NAME);
	particle_tex.loadImage(PARTICLE_TEX);	

	int ox = 10;
	int oy = 10;
	int xNum = ofGetWindowWidth()/ox - 1;
	int yNum = ofGetWindowHeight()/oy - 1;

	numFallen = xNum * yNum;

    _fallens = new fallen*[numFallen];

	for (int i = 0; i < yNum; i++) {
        for (int j = 0; j < xNum; j++) {
            fallen *f = new fallen(j*ox + ox, i*oy + oy, 1, 1);
            _fallens[i*xNum + j] = f;
        }
	}

	printf("%i \n", numFallen);
}

//--------------------------------------------------------------
void initialActivity::update(){
    for (int i = 0; i < numFallen; i++){
        _fallens[i]->update();

        if(_fallens[i]->getVelocity().length() >= 0.1)
            _fallens[i]->applyForce(calculateFriction(frictionCoefficient, *_fallens[i]));
        else {
            _fallens[i]->stop();
        }
    }

//    printf("%f \n", _fallens[0]->getVelocity().x);

    if(forceOn){
        for (int i = 0; i < numFallen; i++){
            ofVec2f f = _forceField->castForce(mouseX, mouseY, *_fallens[i], forceMode);
            //fallenInRange = _forceField.inRange(_fallens[i]->getPosition());
            _fallens[i]->applyForce(f);

        }
    }
}

//--------------------------------------------------------------
void initialActivity::draw(){

	postFx.begin();
	postFx.setFlip(false);

    for (int i = 0; i < numFallen; i++) {
        float speed = _fallens[i]->getVelocity().length();
        float xPos = ofMap(speed, 0, MAX_LENGTH - 5, 0, IMG_WIDTH - 1, false);
       
		pColor = gradImage.getColor(xPos, 10);
		_fallens[i]->draw(pColor);
        _fallens[i]->checkEdge();
        //_fallens[i]->edgeBounce();
    }
	postFx.end();
    //ofDrawFPS(15, 15);
}

ofVec2f initialActivity::calculateFriction(float c, fallen target) {
    friction = target.getVelocity();
    friction *= -1;
    friction.normalize();
    friction *= c;

    return friction;
}

void initialActivity::ofDrawFPS(int x, int y) {
	glPushMatrix();
	glTranslatef(x, y, 0);
	ofSetRectMode(OF_RECTMODE_CORNER);
	ofFill();
	ofSetColor(0x000000);
	ofSetColor(0, 255, 0);
	string fpsStr = "FPS: "+ofToString(ofGetFrameRate(), 2);
	ofDrawBitmapString(fpsStr, 0, 0);
	glPopMatrix();

	ofSetColor(255);
}

//--------------------------------------------------------------
void initialActivity::keyPressed(int key){

}

//--------------------------------------------------------------
void initialActivity::keyReleased(int key){
    ofVec2f force(5,0);
    for (int i = 0; i < numFallen; i++){
        _fallens[i]->applyForce(force);
    }
}

//--------------------------------------------------------------
void initialActivity::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void initialActivity::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void initialActivity::mousePressed(int x, int y, int button){
    forceOn = true;

	if(button == 0){
		forceMode = 1;
	} 

	if(button == 2)
		forceMode = -1;
}

//--------------------------------------------------------------
void initialActivity::mouseReleased(int x, int y, int button){
    forceOn = false;
}

//--------------------------------------------------------------
void initialActivity::windowResized(int w, int h){

}

//--------------------------------------------------------------
void initialActivity::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void initialActivity::dragEvent(ofDragInfo dragInfo){

}
